package tgbot

import (
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
)

type config struct {
	APIToken string
}

func (cfg *config) Flags() *pflag.FlagSet {
	f := pflag.NewFlagSet("TgbotConfig", pflag.PanicOnError)

	f.StringVar(&cfg.APIToken, "api.token", "", "Telegram API token from the botfather")

	return f
}

func (cfg *config) Validate() error {
	if cfg.APIToken == "" {
		return errors.New("telegram API token must be set")
	}

	return nil
}
