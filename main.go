package main

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/the0den/tg-bot/cmd"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: time.RFC3339Nano,
	})
	log.SetOutput(os.Stdout)
}

func main() {
	cmd.Execute()
}
